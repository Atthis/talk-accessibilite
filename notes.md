## A faire sur le site de démo

- nom pour le formulaire
- supprimer le outline: none
- modifier les contrastes
- ajouter un aria-hidden: true aux signes des liens d'articles
- repasser les bouttons en lowercase
- passer les champs en _required_

# Modifications à apporter

## 1- le design

**branche design**

-> Changer le contraste sur les items du menu
|-> Montrer un site de test de contrastes
-> passer les polices de caractère en relatif : rem

## 2- ARIA et HTML sémantique

**branche aria**

**aria-hidden** -> ajouter les attributs aria-hidden aux boutons RS et leur ajouter un aria-label
**semantique** -> remplacer les classes de définition des contenus par des balises html adaptées
--> header
--> nav avec un title
--> main
--> article
--> aside
--> section avec name
--> un name au formulaire
--> footer
--> nav avec title
**titres** -> modifier les hiérarchies des titres

## 3- Boutons et liens

**branche btn-liens**

**btn-article** -> remplacer les boutons d'articles par des balises liens + ajouter à ces boutons un aria-label explicite + tabindex 0
**outline** -> remettre en place le outline des boutons et liens, en le personnalisant un peu pour qu'il soit + visible
**RS-tabindex** -> supprimer les tabindex des boutons de RS

## 4- Formulaire

**branche form**

**inputs** -> Associer les labels aux inputs + ajouter l'attribut _required_ aux inputs
**infos** -> Remplacer le message d'erreurs par un message non-visuel ajouter un aria-label un peu plus explicite au bouton du formulaire
**form-outline** -> ajout d'un outline plus expilcite sur les inputs

## 5- Les images

**branche images**

**alt** -> ajouter un alt aux images de l'article + ajouter un alt au logo du header
